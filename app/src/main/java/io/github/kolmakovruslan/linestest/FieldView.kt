package io.github.kolmakovruslan.linestest

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import io.github.kolmakovruslan.linestest.data.Point
import io.github.kolmakovruslan.linestest.data.PointType
import io.github.kolmakovruslan.linestest.data.forEach

class FieldView(context: Context) : View(context) {

    private val viewModel = FieldViewModel(this)

    private val backgroundRect = Rect()
    private val backgroundPaint = paint { color = Color.LTGRAY }

    private val pointTempRect = Rect()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        viewModel.onMeasure(widthMeasureSpec)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        backgroundRect.set(0, 0, width, width)
        canvas.drawRect(backgroundRect, backgroundPaint)

        viewModel.field.forEach { x, y, type ->
            canvas.drawPoint(x, y, type)
        }

        if (viewModel.startPoint != null) {
            canvas.drawPoint(viewModel.startPoint!!, PointType.START)
        }

        if (viewModel.finishPoint != null) {
            canvas.drawPoint(viewModel.finishPoint!!, PointType.FINISH)
        }
    }

    fun clearFiled() = viewModel.clearFiled()

    fun recreateFiled() = viewModel.recreateFiled()

    override fun onTouchEvent(event: MotionEvent): Boolean {
        viewModel.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    private fun Canvas.drawPoint(point: Point, type: PointType) =
        drawPoint(point.x, point.y, type)

    private fun Canvas.drawPoint(x: Int, y: Int, type: PointType) {
        val left = x * viewModel.pointSize
        val top = y * viewModel.pointSize
        val right = left + viewModel.pointSize
        val bottom = top + viewModel.pointSize
        pointTempRect.set(left, top, right, bottom)
        drawRect(pointTempRect, viewModel.pointPaintByType(type))
    }
}