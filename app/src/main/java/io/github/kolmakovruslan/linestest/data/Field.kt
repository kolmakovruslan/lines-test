package io.github.kolmakovruslan.linestest.data

class Field private constructor(
    private val size: Int,
    private val matrix: Array<Array<PointType>>
) : Cloneable {

    constructor(size: Int) : this(
        size = size,
        matrix = Array(size) { Array(size) { PointType.EMPTY } }
    )

    val itemCount = size * size

    val width = size

    val height = size

    @Throws(ArrayIndexOutOfBoundsException::class)
    operator fun get(x: Int, y: Int): PointType = matrix[x][y]

    @Throws(ArrayIndexOutOfBoundsException::class)
    operator fun set(x: Int, y: Int, type: PointType) {
        matrix[x][y] = type
    }

    public override fun clone(): Field {
        val cloneMatrix =
            Array(size) { x ->
                Array(size) { y ->
                    this[x, y]
                }
            }
        return Field(size, cloneMatrix)
    }
}

inline fun Field.forEach(action: (x: Int, y: Int, type: PointType) -> Unit) {
    for (x in 0 until width) {
        for (y in 0 until height) {
            action(x, y, this[x, y])
        }
    }
}