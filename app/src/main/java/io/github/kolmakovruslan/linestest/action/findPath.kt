package io.github.kolmakovruslan.linestest.action

import io.github.kolmakovruslan.linestest.data.Field
import io.github.kolmakovruslan.linestest.data.Point
import io.github.kolmakovruslan.linestest.data.PointType
import io.github.kolmakovruslan.linestest.getOrNull
import java.util.*

fun Field.findPath(
    start: Point,
    finish: Point
): Field {
    val path = Stack<Point>()
    path.push(start)
    findPath(path, finish, clone())

    val result = clone()
    path.forEach { point ->
        result[point.x, point.y] = PointType.PATH
    }
    return result
}

private fun findPath(
    path: Stack<Point>,
    finish: Point,
    field: Field
) {
    if (path.empty()) return

    val current: Point = path.peek()
    val next: Point? = field.chooseNextPoint(current, finish)

    if (next == null && !path.empty()) {
        field[current.x, current.y] = PointType.DEAD_END
        path.pop()
        return findPath(path, finish, field)
    }

    if (next == null) return

    if (next != finish) {
        field[next.x, next.y] = PointType.PATH
        path.push(next)
        return findPath(path, finish, field)
    }
}

private fun Field.chooseNextPoint(point: Point, finish: Point): Point? {
    val (x, y) = point

    val freePoints = arrayOf(
        pointIfFree(x + 0, y - 1),
        pointIfFree(x + 1, y + 0),
        pointIfFree(x + 0, y + 1),
        pointIfFree(x - 1, y + 0)
    ).filterNotNull()

    val next: Point? = freePoints.minBy {
        val diffX = it.x - finish.x
        val diffY = it.y - finish.y
        val squareX = diffX * diffX
        val squareY = diffY * diffY
        Math.sqrt((squareX + squareY).toDouble())
    }
    return next
}

private fun Field.pointIfFree(x: Int, y: Int): Point? {
    return if (getOrNull(x, y) == PointType.EMPTY) {
        Point(x, y)
    } else {
        null
    }
}