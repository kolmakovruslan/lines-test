package io.github.kolmakovruslan.linestest

import android.graphics.Paint
import android.view.MotionEvent
import android.view.View
import io.github.kolmakovruslan.linestest.action.createField
import io.github.kolmakovruslan.linestest.action.findEmptyPoint
import io.github.kolmakovruslan.linestest.action.findPath
import io.github.kolmakovruslan.linestest.data.Point
import io.github.kolmakovruslan.linestest.data.PointType
import io.github.kolmakovruslan.linestest.data.forEach

class FieldViewModel(
    private val view: FieldView
) {

    var field = createField()
        private set

    var pointSize: Int = 10
        private set

    var startPoint: Point? = null
        private set
    var finishPoint: Point? = null
        private set

    private val pointsPaint = mapOf(
        PointType.EMPTY to paint { color = 0xFFFFDE03.toInt() },
        PointType.WALL to paint { color = 0xFF000000.toInt() },
        PointType.PATH to paint { color = 0xFF008000.toInt() },
        PointType.DEAD_END to paint { color = 0xFF005000.toInt() },
        PointType.START to paint { color = 0xFF0336FF.toInt() },
        PointType.FINISH to paint { color = 0xFFFF0266.toInt() }
    )

    fun clearFiled() {
        field.forEach { x, y, type ->
            if (type != PointType.WALL) field[x, y] = PointType.EMPTY
        }
        startPoint = null
        finishPoint = null
        view.postInvalidate()
    }

    fun recreateFiled() {
        field = createField()
        startPoint = null
        finishPoint = null
        view.postInvalidate()
    }

    fun pointPaintByType(type: PointType): Paint = pointsPaint[type]!!

    fun onMeasure(widthMeasureSpec: Int) {
        pointSize = View.MeasureSpec.getSize(widthMeasureSpec) / field.width
    }

    fun onTouchEvent(event: MotionEvent) {
        when {
            startPoint == null -> {
                startPoint = field.findEmptyPoint(event, pointSize)
                if (startPoint != null) view.postInvalidate()
            }
            finishPoint == null -> {
                finishPoint = field.findEmptyPoint(event, pointSize)
                if (finishPoint != null) view.postInvalidate()
            }
        }
        if (startPoint != null && finishPoint != null) {
            findPath(startPoint!!, finishPoint!!)
        }
    }

    private fun findPath(start: Point, finish: Point) {
        field = field.findPath(start, finish)
        view.postInvalidate()
    }
}