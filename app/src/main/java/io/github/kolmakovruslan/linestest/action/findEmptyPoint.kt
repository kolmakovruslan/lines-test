package io.github.kolmakovruslan.linestest.action

import android.view.MotionEvent
import io.github.kolmakovruslan.linestest.data.Field
import io.github.kolmakovruslan.linestest.data.Point
import io.github.kolmakovruslan.linestest.data.PointType

fun Field.findEmptyPoint(event: MotionEvent, pointSize: Int): Point? {
    var point: Point? = null
    if (event.action == MotionEvent.ACTION_DOWN) {
        val x = (event.x / pointSize).toInt()
        val y = (event.y / pointSize).toInt()

        if (this[x, y] == PointType.EMPTY) {
            point = Point(x, y)
        }
    }
    return point
}