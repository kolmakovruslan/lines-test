package io.github.kolmakovruslan.linestest.data

enum class PointType {
    EMPTY,
    WALL,
    PATH,
    DEAD_END,
    START,
    FINISH
}