package io.github.kolmakovruslan.linestest

import io.github.kolmakovruslan.linestest.data.Field
import io.github.kolmakovruslan.linestest.data.PointType

fun Field.getOrNull(x: Int, y: Int): PointType? {
    return if (x in 0 until width && y in 0 until height) this[x, y] else null
}