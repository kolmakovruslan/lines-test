package io.github.kolmakovruslan.linestest

import android.app.Activity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

class MainActivity : Activity() {

    private lateinit var fieldView: FieldView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fieldView = FieldView(this)
        setContentView(fieldView)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(0, R.id.clear_field, 0, null)
            ?.setIcon(R.drawable.ic_refresh_white_24dp)
            ?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
        menu?.add(0, R.id.recreate_filed, 0, null)
            ?.setIcon(R.drawable.ic_delete_white_24dp)
            ?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.clear_field) {
            fieldView.clearFiled()
        }
        if (item?.itemId == R.id.recreate_filed) {
            fieldView.recreateFiled()
        }
        return super.onOptionsItemSelected(item)
    }
}