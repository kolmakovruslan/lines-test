package io.github.kolmakovruslan.linestest

import android.graphics.Paint

fun paint(init: Paint.() -> Unit): Paint = Paint().apply(init)