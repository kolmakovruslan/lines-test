package io.github.kolmakovruslan.linestest.data

data class Point(val x: Int, val y: Int)