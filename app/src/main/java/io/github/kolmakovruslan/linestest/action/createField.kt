package io.github.kolmakovruslan.linestest.action

import io.github.kolmakovruslan.linestest.data.Field
import io.github.kolmakovruslan.linestest.data.PointType
import java.util.*

fun createField(
    size: Int = 10,
    fillingFactor: Float = 0.2f
): Field {
    val field = Field(size)

    val randomGen = Random(System.currentTimeMillis())

    val wallsCount = ((size * size) * fillingFactor).toInt()

    for (index: Int in 0 until wallsCount)
        field.addWallSomewhere(randomGen)

    return field
}

private fun Field.addWallSomewhere(
    randomGen: Random,
    tryCount: Int = 0
) {
    val x = randomGen.nextInt(width)
    val y = randomGen.nextInt(height)
    if (this[x, y] == PointType.EMPTY || tryCount == itemCount) {
        this[x, y] = PointType.WALL
    } else {
        addWallSomewhere(randomGen, tryCount + 1)
    }
}